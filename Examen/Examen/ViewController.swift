//
//  ViewController.swift
//  Examen
//
//  Created by Jossué Dután on 5/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ViewTitle: UILabel!
    
    @IBOutlet weak var date: UILabel!
    
    @IBOutlet weak var name: UITextField!
    
    @IBAction func next(_ sender: Any) {
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showItems"{
            /*let destination = segue.destination as! ItemInfoViewController
            let selectedRow = itemsTableView.indexPathsForSelectedRows![0]
            destination.itemInfo = (itemManager, selectedRow.row)*/
            
            let destination = segue.destination as! ValoresViewController
            let nameSend = name.text
            destination.nametext = nameSend!
            
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL(string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!){ (data, response, error) in
            guard let data = data else {
                print("Error NO data")
                return
            }
            print(data)
            guard let apiInfo = try? JSONDecoder().decode(APIInfo.self, from: data) else{
                
                print("Error decoding api")
                return
            }
            //print(weatherInfo.weather[0].description)
            
            DispatchQueue.main.async {
                self.ViewTitle.text = "\(apiInfo.viewTitle)"
                self.date.text = "\(apiInfo.date)"

            }
            //self.resultLabel.text = "\"
        }
        task.resume()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


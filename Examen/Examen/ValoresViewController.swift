//
//  ValoresViewController.swift
//  Examen
//
//  Created by Jossué Dután on 5/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import UIKit

class ValoresViewController: UIViewController {

    var nametext:String = ""
    @IBOutlet weak var name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text = "Hi, "+nametext
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

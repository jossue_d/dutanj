//
//  ApiInfo.swift
//  Examen
//
//  Created by Jossué Dután on 5/6/18.
//  Copyright © 2018 Jossué Dután. All rights reserved.
//

import Foundation
/*
struct WradWeatherInfo: Decodable {
    let weather:[WradWeather]
    
}

struct WradWeather: Decodable{
    let id:Int
    let description:String
}
*/
struct APIInfo: Decodable {
    let viewTitle:String
    let date:String
    let nextLink:String
    
}
/*
struct API: Decodable{
    let title:String
    let date:String
    let nextLink:String
}
*/
